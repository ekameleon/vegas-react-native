# VEGAS REACT NATIVE

**Vegas REACT NATIVE** library - **version 0.4.3** is an *Opensource* Library based on **ECMAScript** for develop crossplatform **Rich Internet Applications** and **Games** with React Native.

[![npm version](https://img.shields.io/npm/v/vegas-js-core.svg?style=flat-square?style=flat-square)](https://www.npmjs.com/package/vegas-react-native)
[![npm downloads](https://img.shields.io/npm/dm/vegas-js-core.svg?style=flat-square)](https://www.npmjs.com/package/vegas-react-native)

This library contains a set of libraries writing in **Javascript** and based on the [ES6](http://es6-features.org/) standard.

### About

 * Author : Marc ALCARAZ (aka eKameleon) - Creative Technologist and Digital Architect
 * Mail : ekameleon[at]gmail.com
 * LinkedIn : [https://www.linkedin.com/in/ekameleon/](https://www.linkedin.com/in/ekameleon/)

### Licenses

Under tree opensource licenses :

 * [License MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
 * [License GPL 2.0+](https://www.gnu.org/licenses/gpl-2.0.html)
 * [License LGPL 2.1+](https://www.gnu.org/licenses/lgpl-2.1.html)

## Resources

#### ⌜ Download

Download on **Bitbucket** the latest code, report an issue, ask a question or contribute :

 * [https://bitbucket.org/ekameleon/vegas-react-native](https://bitbucket.org/ekameleon/vegas-react-native)

#### ⌜ Slack Community

![slack-logo-vector-download.jpg](https://bitbucket.org/repo/AEbB9b/images/3509366499-slack-logo-vector-download.jpg)

Send us your email to join the **VEGAS** community on Slack !

## Install

#### ⌜ YARN / NPM

You can install VEGAS REACT NATIVE with [NPM](https://www.npmjs.com/package/vegas-react-native) or [Yarn](https://yarnpkg.com/).

```
$ yarn add vegas-react-native
```

or

```
$ npm install vegas-react-native
```

## Building and test the libraries

**VEGAS React Native** use [Yarn](https://yarnpkg.com/) with a serie of powerful packages (Babel, Mocha, etc.) to compile and build this library.
