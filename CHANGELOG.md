# VEGAS REACT NATIVE - OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.4.3] - 2021-06-30 
- Fix the Storage.run method and use the spread notation run = () => 

## [0.4.2] - 2021-06-30
- Fix the storage helper with the good dependency, use `@react-native-async-storage/async-storage`
- remove the logger dependencies, use only the console if the new `verbose` flag is true.
- Fix bugs and cleaning

## [0.4.1] - 2020-05-24
### Added
- Adds the transitions.Spring class
- Adds the direction property in the Slide component

## [0.3.8] - 2020-03-25
- Adds the vegas-js-logging dependency in the package.json file + replace console by logger.
- Customize the logs with the prefix expression '»»» '

## [0.3.4] - 2020-03-17
- Adds the transitions.Slide component.

## [0.3.3] - 2020-03-17
### Changed
- package.json : "vegas-js-core": "1.0.19"
- package.json : "vegas-js-process": "1.0.4"
  
## [0.3.1] - 2019-10-02
### Changed
- Fix FadeIn + FallDown : rename the disabled property > enabled
- Fix device

## [0.3.0] - 2019-10-01
### Changed
- package.json : react-native version 0.61.1

## [0.2.0] - 2019-09-03
### Added
- local storage package based on @react-native-community/async-storage version 1.6.1

## [0.1.0] - 2019-09-02
### Added
- platform tools : device, mscale, scale, vscale
- Component polyfill
- styles enumerations and tools
- transitions package

