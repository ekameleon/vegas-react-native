
const formatHex = (color) =>
{
    if( color )
    {
        if( color.charAt(1) === "#" )
        {
            return color;
        }
        else
        {
            return "#" + color;
        }
    }
    return null;
};


export default formatHex;
