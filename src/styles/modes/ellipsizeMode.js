/**
 * When numberOfLines is set, this prop defines how text will be truncated. numberOfLines must be set in conjunction with this prop.
 * This can be one of the following values:
 * head - The line is displayed so that the end fits in the container and the missing text at the beginning of the line is indicated by an ellipsis glyph. e.g., "...wxyz"
 * middle - The line is displayed so that the beginning and end fit in the container and the missing text in the middle is indicated by an ellipsis glyph. "ab...yz"
 * tail - The line is displayed so that the beginning fits in the container and the missing text at the end of the line is indicated by an ellipsis glyph. e.g., "abcd..."
 * clip - Lines are not drawn past the edge of the text container.
 * @default 'tail'
 * @type {string}
 */
export const CLIP   = 'clip' ;
export const HEAD   = 'head' ;
export const MIDDLE = 'middle' ;
export const TAIL   = 'tail' ;

export const ellipsizeModes = [ CLIP, HEAD, MIDDLE, TAIL ] ;

export default
{
    CLIP,
    HEAD,
    MIDDLE,
    TAIL
}