export const BACK  = 'back' ;
export const FRONT = 'front' ;
export const SLIDE = 'slide' ;

export const drawerTypes = [ BACK , FRONT , SLIDE ] ;

export default
{
    BACK,
    FRONT,
    SLIDE
}
