export const LEFT  = 'left' ;
export const RIGHT = 'right' ;

export const drawerPositions = [ LEFT , RIGHT ] ;

export default
{
    LEFT,
    RIGHT
}
