const mergeStyles = ( ...styles ) =>
{
    let css = {} ;
    if( styles.length > 0 )
    {
        styles.forEach( ( style ) =>
        {
            if( style instanceof Array )
            {
                style.forEach( style =>
                {
                    css = { ...css , ...style } ;
                }) ;
            }
            else if( style instanceof Object )
            {
                css = { ...css , ...style } ;
            }
        })
    }
    return css ;
};

export default mergeStyles ;