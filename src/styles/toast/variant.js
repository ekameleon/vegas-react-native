export const ERROR   = 'error' ;
export const DEFAULT = 'default' ;
export const SUCCESS = 'success' ;
export const WARNING = 'warning' ;

const variant =
{
    ERROR,
    DEFAULT,
    SUCCESS,
    WARNING
};

export default variant ;

export const variants =
[
    ERROR,
    DEFAULT,
    SUCCESS,
    WARNING
];
