const fontWeight =
{
    '100'   : '100' ,
    '200'   : '200' ,
    '300'   : '300' ,
    '400'   : '400' ,
    '500'   : '500' ,
    '600'   : '600' ,
    '700'   : '700' ,
    '800'   : '800' ,
    
    BOLD    : 'bold',
    NORMAL  : 'normal' 
};

export default fontWeight ;

export const fontWeights =
[
    fontWeight['100'],
    fontWeight['200'],
    fontWeight['300'],
    fontWeight['400'],
    fontWeight['500'],
    fontWeight['600'],
    fontWeight['700'],
    fontWeight['800'],
    fontWeight.BOLD,
    fontWeight.NORMAL
];