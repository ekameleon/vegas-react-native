import mscale from '../../platform/mscale'

const fontSize =
{
     8 :  mscale(8) ,
     9 :  mscale(9) ,
    10 : mscale(10) ,
    11 : mscale(11) ,
    12 : mscale(12) ,
    13 : mscale(13) ,
    14 : mscale(14) ,
    15 : mscale(15) ,
    16 : mscale(16) ,
    17 : mscale(17) ,
    18 : mscale(18) ,
    19 : mscale(19) ,
    20 : mscale(20) ,
    21 : mscale(21) ,
    22 : mscale(22) ,
    23 : mscale(23) ,
    24 : mscale(24) ,
    25 : mscale(25) ,
    26 : mscale(26) ,
    27 : mscale(27) ,
    28 : mscale(28) ,
    29 : mscale(29) ,
    30 : mscale(30) ,
    31 : mscale(31) ,
    32 : mscale(32) ,
    33 : mscale(33) ,
    34 : mscale(34) ,
    40 : mscale(40) ,
    50 : mscale(50) ,
    60 : mscale(60)
};

export default fontSize ;
