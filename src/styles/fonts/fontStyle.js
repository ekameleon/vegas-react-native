const fontStyle =
{
    ITALIC  : 'italic',
    NORMAL  : 'normal'
};

export default fontStyle ;

export const fontStyles =
[
    fontStyle.ITALIC,
    fontStyle.NORMAL
];