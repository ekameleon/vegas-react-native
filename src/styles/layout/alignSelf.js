
export const BASELINE   = 'baseline' ;
export const CENTER     = 'center' ;
export const FLEX_END   = 'flex-end' ;
export const FLEX_START = 'flex-start' ;
export const STRETCH    = 'stretch' ;

const alignSelf =
{
    BASELINE,
    CENTER,
    FLEX_END,
    FLEX_START,
    STRETCH
};

export default alignSelf ;
