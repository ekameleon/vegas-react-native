/**
 * Disabled the wrapping mode.
 * @type {string}
 */
export const NO_WRAP = 'nowrap' ;

/**
 * If wrapping is allowed items are wrapped into multiple lines along the main axis if needed.
 * @type {string}
 */
export const WRAP = 'wrap' ;

const flexWrap =
{
    NO_WRAP,
    WRAP
};

export default flexWrap ;
