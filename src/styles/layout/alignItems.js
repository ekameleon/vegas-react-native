
export const BASELINE   = 'baseline' ;
export const CENTER     = 'center' ;
export const FLEX_END   = 'flex-end' ;
export const FLEX_START = 'flex-start' ;
export const STRETCH    = 'stretch' ;

const alignItems =
{
    BASELINE,
    CENTER,
    FLEX_END,
    FLEX_START,
    STRETCH
};

export default alignItems ;
