/**
 * Align children of a container in the center of the container's main axis.
 * @type {string}
 */
export const CENTER = 'center' ;

/**
 * Align children of a container to the end of the container's main axis.
 * @type {string}
 */
export const FLEX_END = 'flex-end' ;

/**
 * Align children of a container to the start of the container's main axis.
 * @type {string}
 */
export const FLEX_START = 'flex-start' ;

/**
 * Evenly space of children across the container's main axis, distributing remaining space around the children.
 * Compared to space-between using space-around will result in space being distributed to the beginning of the first child and end of the last child.
 * @type {string}
 */
export const SPACE_AROUND  = 'space-around' ;

/**
 * Evenly space of children across the container's main axis, distributing remaining space between the children.
 * @type {string}
 */
export const SPACE_BETWEEN = 'space-between' ;

const justifyContent =
{
    CENTER,
    FLEX_END,
    FLEX_START,
    SPACE_AROUND,
    SPACE_BETWEEN
};

export default justifyContent ;
