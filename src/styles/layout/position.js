/**
 * When positioned absolutely an element doesn't take part in the normal layout flow.
 * It is instead laid out independent of its siblings.
 * The position is determined based on the top, right, bottom, and left values.
 * @type {string}
 */
export const ABSOLUTE = 'absolute' ;

/**
 * By default an element is positioned relatively.
 * This means an element is positioned according to the normal flow of the layout, and then offset relative to that position based on the values of top, right, bottom, and left.
 * The offset does not affect the position of any sibling or parent elements.
 * @type {string}
 */
export const RELATIVE = 'relative' ;

const position =
{
    ABSOLUTE,
    RELATIVE
};

export default position ;
