
export const FLEX = 'flex' ;
export const NONE = 'none' ;

const display =
{
    FLEX,
    NONE
};

export default display ;
