/**
 * Align wrapped lines in the center of the container's cross axis.
 * @type {string}
 */
export const CENTER = 'center' ;

/**
 * wrapped lines to the end of the container's cross axis.
 * @type {string}
 */
export const FLEX_END = 'flex-end' ;

/**
 * Align wrapped lines to the start of the container's cross axis.
 * @type {string}
 */
export const FLEX_START = 'flex-start' ;

/**
 * wrapped lines to match the height of the container's cross axis.
 * @type {string}
 */
export const STRETCH = 'stretch' ;

/**
 * Evenly space wrapped lines across the container's main axis, distributing remaining space around the lines.
 * Compared to space between using space around will result in space being distributed to the begining of the first lines and end of the last line.
 * @type {string}
 */
export const SPACE_AROUND = 'space-around' ;

/**
 * Evenly space wrapped lines across the container's main axis, distributing remaining space between the lines.
 * @type {string}
 */
export const SPACE_BETWEEN = 'space-between' ;

const alignContent =
{
    CENTER,
    FLEX_END,
    FLEX_START,
    STRETCH,
    SPACE_AROUND,
    SPACE_BETWEEN
};

export default alignContent ;

