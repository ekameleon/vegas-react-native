const layout =
{
    center:
    {
        alignItems     : 'center',
        justifyContent : 'center'
    },
    column :
    {
        flex          : 1,
        flexDirection : 'column'
    },
    row :
    {
        flex          : 1 ,
        flexDirection : 'row'
    },
    rowWrap :
    {
        flex          : 1 ,
        flexDirection : 'row' ,
        flexWrap      : 'wrap'
    }
};

export default layout ;
