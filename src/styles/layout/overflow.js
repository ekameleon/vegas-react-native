
export const HIDDEN  = 'hidden' ;
export const SCROLL  = 'scroll' ;
export const VISIBLE = 'visible' ;

const overflow =
{
    HIDDEN,
    SCROLL,
    VISIBLE
};

export default overflow ;
