/**
 * Align children from top to bottom. If wrapping is enabled then the next line will start to the left first item on the top of the container.
 * @type {string}
 */
export const COLUMN = 'column' ;

/**
 * Align children from bottom to top. If wrapping is enabled then the next line will start to the left first item on the bottom of the container.
 * @type {string}
 */
export const COLUMN_REVERSE = 'column-reverse' ;

/**
 * Align children from left to right. If wrapping is enabled then the next line will start under the first item on the left of the container.
 * @type {string}
 */
export const ROW = 'row' ;

/**
 * Align children from right to left. If wrapping is enabled then the next line will start under the first item on the right of the container.
 * @type {string}
 */
export const ROW_REVERSE = 'row-reverse' ;

const flexDirection =
{
    COLUMN,
    COLUMN_REVERSE,
    ROW,
    ROW_REVERSE
};

export default flexDirection ;
