const textAlign =
{
    AUTO    : 'auto' ,
    CENTER  : 'center',
    JUSTIFY : 'justify' , // only IOS for the moment
    LEFT    : 'left' ,
    RIGHT   : 'right'
};

export default textAlign ;

export const textAligns =
[
    textAlign.AUTO,
    textAlign.CENTER,
    textAlign.JUSTIFY,
    textAlign.LEFT,
    textAlign.RIGHT
];