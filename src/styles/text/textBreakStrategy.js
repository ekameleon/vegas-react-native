const textBreakStrategy =
{
    BALANCED      : 'balanced' ,
    HIGHT_QUALITY : 'highQuality',
    SIMPLE        : 'simple'
};

export default textBreakStrategy ;

export const textBreakStrategies =
[
    textBreakStrategy.BALANCED,
    textBreakStrategy.HIGHT_QUALITY,
    textBreakStrategy.SIMPLE
];