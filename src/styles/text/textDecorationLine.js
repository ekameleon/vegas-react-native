const textDecorationLine =
{
    LINE_THROUGH           : 'line-through',
    NONE                   : 'none' ,
    UNDERLINE              : 'underline' ,
    UNDERLINE_LINE_THROUGH : 'underline line-through'
};

export default textDecorationLine ;

export const textDecorationLines =
[
    textDecorationLine.LINE_THROUGH,
    textDecorationLine.NONE,
    textDecorationLine.UNDERLINE,
    textDecorationLine.UNDERLINE_LINE_THROUGH
];

