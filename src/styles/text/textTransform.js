// 'none', 'uppercase', 'lowercase', 'capitalize'
const textTransform =
{
    CAPITALIZE : 'capitalize',
    LOWERCASE  : 'lowercase' ,
    NONE       : 'none' ,
    UPPERCASE  : 'uppercase'
};

export default textTransform ;

export const textTransforms =
[
    textTransform.CAPITALIZE ,
    textTransform.LOWERCASE  ,
    textTransform.NONE       ,
    textTransform.UPPERCASE
];

