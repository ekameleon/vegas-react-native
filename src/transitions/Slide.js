import React , { PureComponent } from 'react'

import PropTypes from 'prop-types'

import { Animated } from 'react-native'

class Slide extends PureComponent
{
    constructor( props )
    {
        super( props ) ;
        this.value = new Animated.Value(0)
    }

    animate = () =>
    {
        const {
            bounciness ,
            delay,
            enabled,
            from,
            speed,
            to:toValue,
            useNativeDriver
        } = this.props ;

        if( enabled )
        {
            this.value.setValue( from ) ;
            Animated.spring( this.value ,
            {
                bounciness,
                delay,
                speed,
                toValue,
                useNativeDriver
            }).start() ;
        }
        else
        {
            this.value.setValue( toValue ) ;
        }
    };

    componentDidMount() { this.animate() ; }

    render()
    {
        const { children, direction, zIndex } = this.props ;
        const property = direction === 'vertical' ? 'translateY' : 'translateX' ;
        return (
            <Animated.View
                style ={
                {
                    transform :
                    [ { [property] : this.value } ] ,
                    ...{ zIndex }
                }}
            >
                { children }
            </Animated.View>
        )
    }
}

Slide.defaultProps =
{
    bounciness      : 6 ,
    delay           : 250 ,
    direction       : 'horizontal' ,
    enabled         : true ,
    from            : -200 ,
    speed           : 3 ,
    to              : 0 ,
    useNativeDriver : true ,
    zIndex          : 1
};

Slide.propTypes =
{
    bounciness      : PropTypes.number,
    delay           : PropTypes.number,
    direction       : PropTypes.oneOf(['horizontal','vertical']),
    enabled         : PropTypes.bool,
    from            : PropTypes.number,
    speed           : PropTypes.number,
    to              : PropTypes.number,
    useNativeDriver : PropTypes.bool,
    zIndex          : PropTypes.number
};

export default Slide ;