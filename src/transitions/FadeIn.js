import React , { PureComponent } from 'react'

import PropTypes from 'prop-types'

import { Animated , Easing } from 'react-native'

import mergeStyles from '../styles/mergeStyles'

class FadeIn extends PureComponent
{
    constructor( props )
    {
        super( props ) ;
        this.value = new Animated.Value(0) ;
    }

    animate = () =>
    {
        const {
            delay,
            duration,
            enabled,
            easing,
            from,
            to
        } = this.props ;

        if( enabled )
        {
            this.value.setValue( from ) ;
            Animated.timing( this.value ,
            {
                easing   : easing ,
                delay    : delay,
                duration : duration ,
                toValue  : to
            })
            .start() ;
        }
        else
        {
            this.value.setValue( to ) ;
        }
    };

    componentDidMount() { this.animate() ; }

    render()
    {
        const { children , style, ...rest } = this.props ;
        const css = { opacity:this.value };
        return (
            <Animated.View style={ mergeStyles(css,style) } { ...rest } >
                { children }
            </Animated.View>
        );
    }
}

FadeIn.defaultProps =
{
    delay    : 250,
    duration : 2000,
    enabled  : true ,
    easing   : Easing.inOut(Easing.back),
    from     : 0 ,
    style    : null ,
    to       : 0.6
};

FadeIn.propTypes =
{
    delay    : PropTypes.number,
    duration : PropTypes.number,
    enabled  : PropTypes.bool,
    easing   : PropTypes.func,
    from     : PropTypes.number,
    style    : PropTypes.oneOfType([PropTypes.object,PropTypes.array]) ,
    to       : PropTypes.number
};

export default FadeIn ;
