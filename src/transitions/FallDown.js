import React , { PureComponent } from 'react'

import PropTypes from 'prop-types'

import { Animated } from 'react-native'

class FallDown extends PureComponent
{
    constructor( props )
    {
        super( props ) ;
        this.value = new Animated.Value(0)
    }

    animate = () =>
    {
        const {
            bounciness ,
            delay,
            enabled,
            speed,
            top,
            toValue,
            useNativeDriver
        } = this.props ;

        const config = { bounciness, speed, toValue, useNativeDriver };

        if( enabled )
        {
            this.value.setValue( top ) ;
            Animated.sequence(
            [
                Animated.delay( delay ) ,
                Animated.spring( this.value , config )
            ]).start() ;
        }
        else
        {
            this.value.setValue( toValue ) ;
        }
    };

    componentDidMount() { this.animate() ; }

    render()
    {
        const { children, zIndex } = this.props ;
        return (
            <Animated.View style={{ transform : [{ translateY : this.value }] , ...{ zIndex } }}>
                { children }
            </Animated.View>
        )
    }
}

FallDown.defaultProps =
{
    bounciness      : 6 ,
    delay           : 250,
    enabled         : true ,
    speed           : 3 ,
    top             : -200 ,
    toValue         : 0 ,
    useNativeDriver : true ,
    zIndex          : 1
};

FallDown.propTypes =
{
    bounciness      : PropTypes.number,
    delay           : PropTypes.number,
    enabled         : PropTypes.bool,
    speed           : PropTypes.number,
    top             : PropTypes.number,
    toValue         : PropTypes.number ,
    useNativeDriver : PropTypes.bool,
    zIndex          : PropTypes.number
};

export default FallDown ;
