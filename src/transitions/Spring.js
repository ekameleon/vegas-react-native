import React , { PureComponent } from 'react'

import PropTypes from 'prop-types'

import { Animated } from 'react-native'

class Spring extends PureComponent
{
    constructor( props )
    {
        super( props ) ;
        this.value = new Animated.Value(0);
    }

    animate = () =>
    {
        const {
            bounciness ,
            damping,
            delay,
            enabled,
            from,
            friction,
            mass,
            speed,
            stiffness,
            tension,
            to:toValue,
            useNativeDriver
        } = this.props ;

        if( enabled )
        {
            this.value.setValue( from ) ;
            Animated.spring( this.value ,
            {
                bounciness,
                damping,
                delay,
                friction,
                mass,
                speed,
                stiffness,
                tension,
                toValue,
                useNativeDriver
            })
            .start() ;
        }
        else
        {
            this.value.setValue( toValue ) ;
        }
    };

    componentDidMount() { this.animate() ; }

    render()
    {
        const { children, direction, zIndex } = this.props ;
        const property = direction === 'vertical' ? 'translateY' : 'translateX' ;
        return (
            <Animated.View 
                style ={
                {
                    transform : [ { [ property ] : this.value } ] ,
                    ...{ zIndex }
                }}
            >
                { children }
            </Animated.View>
        )
    }
}

Spring.defaultProps =
{
    enabled         : true ,
    from            : -200 ,
    member          : 'translateX',
    to              : 0 ,
    useNativeDriver : true ,
    zIndex          : 1
};

Spring.propTypes =
{
    bounciness      : PropTypes.number,
    damping         : PropTypes.number,
    delay           : PropTypes.number,
    enabled         : PropTypes.bool,
    friction        : PropTypes.number,
    from            : PropTypes.number,
    member          : PropTypes.string.isRequired,
    mass            : PropTypes.number,
    speed           : PropTypes.number,
    stiffness       : PropTypes.number,
    tension         : PropTypes.number,
    to              : PropTypes.number,
    useNativeDriver : PropTypes.bool,
    zIndex          : PropTypes.number
};

export default Slide ;
