import all        from './all'
import clear      from './clear'
import getItem    from './getItem'
import removeItem from './removeItem'
import setItem    from './setItem'
import store      from './store'

const storage =
{
    all,
    clear,
    getItem,
    removeItem,
    setItem,
    store
};

export default storage ;
