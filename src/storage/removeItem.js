import AsyncStorage from '@react-native-async-storage/async-storage'

import cache from './cache'

const removeItem = ( key , verbose = false ) =>
{
    cache.remove(key) ;
    AsyncStorage
        .removeItem(key)
        .then( () =>
        {
            if( verbose )
            {
                console.log('»»» storage removeItem succeed.')
            }
        })
        .catch( () =>
        {
            if( verbose )
            {
                console.warn('»»» storage removeItem failed.' ) ;
            }
        }) ;
};

export default removeItem ;
