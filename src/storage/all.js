import AsyncStorage from '@react-native-async-storage/async-storage'

import cache from './cache'

const all = ( verbose = false )  =>
{
    return AsyncStorage
        .getAllKeys()
        .then( keys =>
        {
            const promiseAll = keys
                .map( key => AsyncStorage
                    .getItem(key)
                    .then(data => JSON.parse(data))
                    .then(data =>
                    {
                        cache.set( key, data ) ;
                        return { [key] : data } ;
                    })
                );

            return Promise
                .all(promiseAll)
                .then( datas =>
                {
                    let flatData = {} ;
                    datas.forEach( data =>
                    {
                        flatData = { ...flatData, ...data } ;
                    }) ;
                    return flatData ;
                })
                .catch( error =>
                {
                    if( verbose )
                    {
                        console.warn( '»»» all storage failed, error:' + error.toString() )
                    }
                }) ;
        }) ;
};

export default all ;
