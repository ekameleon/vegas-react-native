import cache from './cache'

const store = () => cache.all() ;

export default store ;
