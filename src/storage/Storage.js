import Action from 'vegas-js-process/src/Action'

import defaultStorage from './index.js'

/**
 * The local data storage helper of the application.
 */
class Storage extends Action
{
    /**
     * Creates a new Storage instance.
     * @param {Object} option The optional setting definition of the storage instance.
     * @param {Object} option.storage The storage helper reference (or use the default storage)
     * @param {boolean} option.verbose Indicates if the log messages are displayed in the console.
     */
    constructor( { storage = defaultStorage , verbose = false } = {} )
    {
        super() ;
        this._stored = false ;
        this.storage = storage ;
        this.verbose = verbose ;
        this.notifyFinished = this.notifyFinished.bind(this) ;
        this.notifyStarted  = this.notifyStarted.bind(this) ;
    }

    get length()
    {
        if( this._stored )
        {
            return Object.keys(this.storage.store()).length ;
        }
        return 0 ;
    }

    /**
     * Indicates if the storage is stored.
     */
    get stored() { return this._stored; }

    /**
     * Clear all key/value pairs registered in the storage object.
     * @returns {Storage} The current Storage reference.
     */
    clear = () =>
    {
        if( this.storage )
        {
            this.storage.clear() ;
        }
        return this ;
    };

    /**
     * Fetches an item for a key.
     * @param key {string|null} Key of the item to fetch.
     * @returns {*} A value or null.
     */
    getItem = key =>
    {
        if( this._stored )
        {
            if( key === null )
            {
                this.warn( this + ' getItem failed because key is null' ) ;
                return null ;
            }
            let object = null ;
            if( this.storage )
            {
                object = this.storage.getItem( key ) ;
            }
            return object ? JSON.parse(object) : null ;
        }
        return null ;
    };

    /**
     * Removes an item for a key
     * @param key {string} Key of the item to remove.
     * @returns {Storage} The current Storage reference.
     */
    removeItem = key =>
    {
        if( this._stored )
        {
            if( key === null )
            {
                this.warn( this + ' removeItem failed because key is null' );
                return null ;
            }
            if( this.storage )
            {
                this.storage.removeItem( key ) ;
            }
        }
        return this ;
    };

    /**
     * Run the process.
     * @memberof com.ooopener.process.Storage
     * @function
     * @instance
     */
    run = () =>
    {
        this.notifyStarted();
        if( this.storage )
        {
            this.storage
            .all()
            .then( () =>
            {
                this._stored = true ;
                this.notifyFinished()
            })
            .catch( err =>
            {
                this.warn( this + " run failed, impossible to initialize the storage, error" + err.toString() ) ;
                this.notifyFinished() ;
            });
            return ;
        }
        this.warn( this + " run failed, the internal storage reference not must be null or undefined.") ;
        this.notifyFinished() ;
    }

    /**
     * Sets the value for a key.
     * @param key {string} Key of the item to fetch.
     * @param value {string} Value to set for the key.
     * @returns {Storage} The current Storage reference.
     */
    setItem = ( key , value ) =>
    {
        if( this._stored )
        {
            if( key === null || value === null )
            {
                this.warn( this + ' setItem failed because key and/or value is null' );
                return null ;
            }
            if( this.storage )
            {
                this.storage.setItem( key , JSON.stringify(value) );
            }
        }
        return this ;
    };

    warn = message =>
    {
        if( !!(this.verbose) )
        {
            console.warn( message ) ;
        }
    };
}

export default Storage ;
