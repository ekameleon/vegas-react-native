import cache from './cache'

const getItem = key => cache.get(key) ;

export default getItem ;
