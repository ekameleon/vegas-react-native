import AsyncStorage from '@react-native-async-storage/async-storage'

import cache from './cache'

const setItem = ( key , value , verbose = false ) =>
{
    cache.set(key, value) ;
    AsyncStorage
        .setItem( key , JSON.stringify(value) )
        .then( () =>
        {
            if( verbose )
            {
                console.log('»»» storage setItem succeed.')
            }
        })
        .catch( ()  =>
        {
            if ( verbose )
            {
                console.warn( '»»» storage setItem failed.' );
            }
        }) ;
};

export default setItem ;
