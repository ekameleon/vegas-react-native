const namespace = 'appStore' ;

const all = () => Promise.resolve(JSON.parse( localStorage[namespace]) ) ;

const clear = () =>
{
    delete localStorage[namespace] ;
    init( namespace ) ;
    return storage ;
};

const getItem = key =>
{
    const items = store() ;
    return items[key] ;
};

const init = namespace =>
{
    if (typeof localStorage !== 'undefined')
    {
        localStorage[namespace] = localStorage[namespace] ? localStorage[namespace] : '{}' ;
    }
};

const removeItem = key =>
{
    const items = store() ;
    delete items[key] ;
    localStorage[namespace] = JSON.stringify(items) ;
    return storage ;
};

const setItem = ( key , value ) =>
{
    const items = store() ;
    items[key] = value ;
    localStorage[namespace] = JSON.stringify(items) ;
    return storage ;
};


const store = () => JSON.parse(localStorage[namespace]) ;

init( namespace ) ;

/*
 TODO:
 - missing "key" support
 - missing "length" support
 */
const storage = {
    all,
    clear,
    getItem,
    removeItem,
    setItem,
    store
};

export default storage ;




