let store = {} ;

const all = () => store ;

const clear = () =>
{
    store = {} ;
};

const get = key => store[key] ;

const remove = key =>
{
    delete store[key] ;
};

const set = ( key , value ) =>
{
    store[key] = value ;
};

/**
 * Local cache object.
 */
const cache = {
    all,
    clear,
    set,
    get,
    remove
};

export default cache ;
