import AsyncStorage from '@react-native-async-storage/async-storage'

import cache from './cache'

const clear = ( verbose = false )  =>
{
    cache.clear() ;
    AsyncStorage
        .clear()
        .then( () =>
        {
            if( verbose )
            {
                console.debug('»»» storage clear succeed.')
            }
        })
        .catch( () =>
        {
            if( verbose )
            {
                console.warn( '»»» storage clear failed.' ) ;
            }
        }) ;
};

export default clear ;
