import { Dimensions } from 'react-native'

const { height , width } = Dimensions.get('window');

const dim = (width < height) ? height : width;

/**
 * Will return linear scaled result of the provided size, based on your device's screen height.
 * @module
 * @param {number} size - The size to scale based on your device's screen height.
 * @returns {number} The linear scaled result.
 */
const vscale = size => dim / 680 * size ;

export default vscale ;

