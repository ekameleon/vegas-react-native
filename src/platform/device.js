import { Platform, Dimensions, PixelRatio } from 'react-native'

import mscale from './mscale'
import scale  from './scale'
import vscale from './vscale'

const { width , height } = Dimensions.get("window");

const os = Platform.OS ;

const android = os === "android" ;
const ios     = os === "ios" ;
const macos   = os === "macos" ;
const web     = os === "web" ;
const windows = os === "windows" ;

const iphone2G     = ios && (height === 480 || width === 480);
const iphone3G     = ios && (height === 480 || width === 480);
const iphone3GS    = ios && (height === 480 || width === 480);
const iphone4      = ios && (height === 480 || width === 480);
const iphone4s     = ios && (height === 480 || width === 480);
const iphone5      = ios && (height === 568 || width === 568);
const iphone5s     = ios && (height === 568 || width === 568);
const iphone5c     = ios && (height === 568 || width === 568);
const iphoneSE     = ios && (height === 568 || width === 568);
const iphone6      = ios && (height === 667 || width === 667);
const iphone6s     = ios && (height === 667 || width === 667);
const iphone6Plus  = ios && (height === 736 || width === 736);
const iphone6sPlus = ios && (height === 736 || width === 736);
const iphone7      = ios && (height === 667 || width === 667);
const iphone7Plus  = ios && (height === 736 || width === 736);
const iphone8      = ios && (height === 667 || width === 667);
const iphone8Plus  = ios && (height === 736 || width === 736);
const iphoneX      = ios && (height === 812 || width === 812);
const iphoneXr     = ios && (height === 896 || width === 896);
const iphoneXs     = ios && (height === 812 || width === 812);
const iphoneXsMax  = ios && (height === 896 || width === 896);

const isIphoneX = iphoneX || iphoneXs || iphoneXsMax || iphoneXr ;

const hp = value => Math.round( (value * height) / 100) ;
const wp = value => Math.round( (value * width)  / 100) ;

const device =
{
    width,
    height,

    hp,
    mscale ,
    scale  ,
    vscale ,
    wp,

    fontRatio  : PixelRatio.getFontScale() ,
    pixelRatio : PixelRatio.get() ,

    os ,

    android ,
    macos,
    ios,
    web,
    windows,

    iphone2G,
    iphone3G,
    iphone3GS,
    iphone4,
    iphone4s,
    iphone5,
    iphone5c,
    iphone5s,
    iphoneSE,
    iphone6,
    iphone6s,
    iphone6Plus,
    iphone6sPlus,
    iphone7,
    iphone7Plus,
    iphone8,
    iphone8Plus,
    iphoneX,
    iphoneXr,
    iphoneXs,
    iphoneXsMax,
    isIphoneX,

    iphoneXInset :
    {
        landscape :
        {
            bottom : 21,
            left   : 44,
            right  : 44,
            top    : 0
        },
        portrait :
        {
            bottom : 34,
            left   : 0,
            right  : 0,
            top    : 24
        }
    }
};

export default device ;
