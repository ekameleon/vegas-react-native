import { Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window');

const dim = width < height ? width : height ;

/**
 * Will return linear scaled result of the provided size, based on your device's screen width.
 * @module
 * @param {number} size The value to transform with your device's screen width ratio.
 * @returns {number} The linear scaled result.
 */
export default (size) => dim / 350 * size ;
