import round from 'vegas-js-core/src/maths/round'

import scale from './scale'

/**
 * Sometimes you don't want to scale everything in a linear manner, that's where moderate scale comes in.
 * This method provides an helper you can control the resize factor (default is 0.5).
 * @param {number} size - The size to transform.
 * @param {number} [factor=0.5] - The factor ratio to moderate the scaling of the specific value.
 * @param {number} [floatCount=0] - The count of number after the point.
 * @return {number} A moderated scale value.
 */
const mscale = ( size , factor = 0.5 , floatCount = 0 ) => round(size + ( scale(size) - size ) * factor, floatCount ) ;

export default mscale ;
