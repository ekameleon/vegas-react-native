module.exports = {
    
    root: true,
    extends: '@react-native-community',
  
    parser : "babel-eslint",
    parserOptions : 
    {
        "ecmaVersion"  : 2018,
        "sourceType"   : "module",
        "ecmaFeatures" : 
        {
            "jsx" : true,
            "modules" : true,
            "experimentalObjectRestSpread" : true
        }
    },
    plugins : 
    [
        "react"
    ],
    rules : 
    {
        "comma-dangle": 0,
        
        "react/jsx-uses-react": 1,
        "react/jsx-uses-vars": 1,
        "react/display-name": 0,

        "no-console": "off",
        "no-unused-vars": "warn",
        "no-unexpected-multiline": "warn",
        
        "semi": "off"
    },
    settings: 
    {
        "react": 
        {
            "pragma": "React",
            "version": "16.13.0"
        }
    }
};
